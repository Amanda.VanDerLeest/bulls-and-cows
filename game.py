from random import randint  #we need this because random is an external library and from that we need to import randint to be able to use randint during the code

# Generates a list of four, unique, single-digit numbers
def generate_secret():      #function is called generate secret
    my_list = []           #first thing we initilize a list called my list
    while len(my_list) < 4: #we make this while loop and we call it if the list is less then 4
        n = randint(0,9)        #make a value n and call a random integer in the range of 0,9. we know we need 4 unique single digit numbers. so we know they need to be between 0 and 9. by doing 0,9 it starts at 1-9. its "not includive"
        if n not in my_list:    #if n has not already been added to my list, then go ahead and add it. then it will reloop up 4 times but on the 5th time the condition will no longer be true
            my_list.append(n)
    return my_list

def parse_numbers(s):
    l = []
    for n in s:
        number = int(n)
        l.append(number)
    print ("l: ", l)
    return l

def count_exact_matches(first, second):
    count = 0
    for a, b in zip(first, second):
        if a == b:
            count = count + 1
    return count

def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    number_of_guesses = 20

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        # :
        # while the length of their response is not equal
        # to 4,
        #   tell them they must enter a four-digit number
        #   prompt them for the input, again

        while len(guess) != 4:  #!= means does not equal
            print("you must enter a four-digit number")
            guess = input(prompt)

        # :
        # write a function and call it here to:
        # convert the string in the variable guess to a list
        # of four single-digit numbers

        converted_guess = parse_numbers(guess)

        # :
        # write a function and call it here to:
        # count the number of exact matches between the
        # converted guess and the secret
        count_exact_matches(converted_guess, secret)

        # :
        # write a function and call it here to:
        # count the number of common entries between the
        # converted guess and the secret

        # :
        # if the number of exact matches is four, then
        # the player has correctly guessed! tell them
        # so and return from the function.

        # :
        # report the number of "bulls" (exact matches)
        # :
        # report the number of "cows" (common entries - exact matches)

    # :
    # If they don't guess it in 20 tries, tell
    # them what the secret was.


# Runs the game
def run():
    play_game()
    # : Delete the word pass, below, and have this function:
    # Call the play_game function
    # Ask if they want to play again
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again


if __name__ == "__main__":
    run()
